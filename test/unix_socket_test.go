package test

import (
	"fmt"
	"net"
	"os"
	"testing"
)

const socketPath = "/Users/oker/logs/temp/unix_socket_example"

// TestServer 测试服务端
//
//	@Description: 测试服务端
//	@param t
func TestServer(t *testing.T) {
	if _, err := os.Stat(socketPath); err == nil {
		os.Remove(socketPath) // 如果文件存在，删除文件
	}
	listener, err := net.Listen("unix", socketPath) // 监听,创建一个unix socket
	if err != nil {
		fmt.Println("监听发生异常:", err)
		return
	}
	defer listener.Close() // 关闭监听
	fmt.Println("服务启动...")
	for { // 循环
		conn, err := listener.Accept() // 接收连接
		if err != nil {
			fmt.Println("接受信息错误:", err)
			return
		}
		go handleConnection(conn) // 处理连接
	}
}

// handleConnection 处理连接
//
//	@Description: 处理连接
//	@param conn
func handleConnection(conn net.Conn) {
	defer conn.Close()           // 关闭连接
	buffer := make([]byte, 1024) // 创建一个缓冲区
	n, err := conn.Read(buffer)  // 读取数据
	if err != nil {              // 如果读取错误
		fmt.Println("读取错误:", err)
		return
	}
	conn.Write([]byte("Hello FunTester"))        // 写入数据
	fmt.Printf("收到消息: %s\n", string(buffer[:n])) // 打印接收到的数据
}

// TestClient 测试客户端
//
//	@Description: 测试客户端
//	@param t
func TestClient(t *testing.T) {
	conn, err := net.Dial("unix", socketPath)
	if err != nil {
		fmt.Println("拨号发生错误:", err)
		return
	}
	defer conn.Close()
	message := "Hello FunTester"
	_, err = conn.Write([]byte(message))
	if err != nil {
		fmt.Println("写入消息错误:", err)
		return
	}
}
