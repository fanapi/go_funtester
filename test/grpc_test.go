package test

import (
	"context"
	"fmt"
	pb "funtester/proto"
	"google.golang.org/grpc"
	"net"
	"testing"
)

type server struct {
	pb.UnimplementedHelloServer
}

func (s *server) Say(ctx context.Context, req *pb.SayRequest) (*pb.SayResponse, error) {
	fmt.Println("request:", req.Name)
	return &pb.SayResponse{Message: "Hello " + req.Name}, nil
}

func TestGpc(t *testing.T) {
	listen, err := net.Listen("tcp", ":8001")
	if err != nil {
		fmt.Printf("failed to listen: %v", err)
		return
	}
	s := grpc.NewServer()
	pb.RegisterHelloServer(s, &server{})
	//reflection.Register(s)

	defer func() {
		s.Stop()
		listen.Close()
	}()

	fmt.Println("Serving 8001...")
	err = s.Serve(listen)
	if err != nil {
		fmt.Printf("failed to serve: %v", err)
		return
	}
}
